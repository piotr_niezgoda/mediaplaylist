package soh.bykowsky;

public class Movie implements IPlayable{
    private String title;
    private String director;
    private double duration;

    public Movie() {
    }

    public Movie(String title, String director, double duration) {
        this.title = title;
        this.director = director;
        this.duration = duration;
    }

    @Override
    public boolean playElement() {
        System.out.println("Film: " + this.title);
        return true;
    }

    // getters and setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}


