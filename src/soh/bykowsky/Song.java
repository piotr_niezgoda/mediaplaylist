package soh.bykowsky;

public class Song implements IPlayable{
    private String title;
    private String album;
    private String author;
    private double duration;

    public Song() {
    }

    public Song(String title, String album, String author, double duration) {
        this.title = title;
        this.album = album;
        this.author = author;
        this.duration = duration;
    }

    @Override
    public boolean playElement() {
        System.out.println("Muzyka: " + this.author + ", " + this.title);
        return true;
    }

    // getters and setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
