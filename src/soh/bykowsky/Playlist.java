package soh.bykowsky;

import java.util.*;

public class Playlist implements IPlayable, IPlaylist {
    private List<Integer> playlist;
    private List<Integer> shuffledPlayList;
    private List<IPlayable> mediaLibrary;
    private ListIterator<Integer> listIterator;

    private PlaybackMode playbackMode;
    private String playlistTitle;


    public Playlist(List<IPlayable> mediaLibrary) {
        this.playbackMode = PlaybackMode.NORMAL;    // Let's make default mode
        this.mediaLibrary = mediaLibrary;
        this.playlist = new ArrayList<>();
    }

    public Playlist(List<IPlayable> mediaLibrary, PlaybackMode playbackMode) {
        this.playbackMode = playbackMode;
        this.mediaLibrary = mediaLibrary;
        this.playlist = new ArrayList<>();

        if (shuffledPlayList!= null && playbackMode == PlaybackMode.RANDOM){
            Collections.shuffle(shuffledPlayList);
        }
    }

    public Playlist(List<IPlayable> mediaLibrary, PlaybackMode playbackMode, List<Integer> playlist) {
        this.playlist = playlist;
        this.playbackMode = playbackMode;
        this.mediaLibrary = mediaLibrary;

        if (playbackMode == PlaybackMode.RANDOM){
            List<Integer> shuffledPlaylist = playlist;
            Collections.shuffle(shuffledPlaylist);
            this.shuffledPlayList = shuffledPlaylist;
        }

        this.listIterator = playlist.listIterator();
    }

    @Override
    public boolean playElement() {

        if (listIterator.hasNext()) {

            IPlayable elementToPlay = mediaLibrary.get(listIterator.next()-1);

            if (elementToPlay instanceof Playlist){
                Playlist elementToPlayAsPlaylist = (Playlist)elementToPlay;

                ListIterator<Integer> innerListIterator = elementToPlayAsPlaylist.getListIterator();
                if (innerListIterator.hasNext()){
                    listIterator.previous();

                    mediaLibrary.get(innerListIterator.next()-1).playElement();
                    return true;
                } else if (elementToPlayAsPlaylist.getPlaybackMode() == PlaybackMode.LOOP){
                    listIterator.previous();

                    elementToPlayAsPlaylist.setListIterator(((Playlist) elementToPlay).getPlaylist().listIterator());   // reset inner iterator
                    mediaLibrary.get(innerListIterator.next()-1).playElement();
                    return true;
                }
            }

            elementToPlay.playElement();
            return true;

        } else if (playbackMode == PlaybackMode.LOOP) {
            listIterator = playlist.listIterator();       // resets the iterator
            IPlayable elementToPlay = mediaLibrary.get(listIterator.next()-1);
            elementToPlay.playElement();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void addElementToPlaylist(int mediaLibraryElementIndexTo) {
        this.playlist.add(mediaLibraryElementIndexTo);
        listIterator = playlist.listIterator();
    }

    @Override
    public void removeElementFromPlaylist(int elementIndexInPlaylist) {
        this.playlist.remove(elementIndexInPlaylist);
        listIterator = playlist.listIterator();
    }


    // getters and setters
    public PlaybackMode getPlaybackMode() {
        return playbackMode;
    }
    public void setPlaybackMode(PlaybackMode playbackMode) {
        this.playbackMode = playbackMode;

        if (playbackMode == PlaybackMode.RANDOM && this.shuffledPlayList != null){
            Collections.shuffle(this.shuffledPlayList);
        }
    }

    public String getPlaylistTitle() {
        return playlistTitle;
    }

    public void setPlaylistTitle(String playlistTitle) {
        this.playlistTitle = playlistTitle;
    }

    public List<IPlayable> getMediaLibrary() {
        return mediaLibrary;
    }

    public void setMediaLibrary(List<IPlayable> mediaLibrary) {
        this.mediaLibrary = mediaLibrary;
    }

    public List<Integer> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(List<Integer> playlist) {
        this.playlist = playlist;
    }

    public List<Integer> getShuffledPlayList() {
        return shuffledPlayList;
    }

    public void setShuffledPlayList(List<Integer> shuffledPlayList) {
        this.shuffledPlayList = shuffledPlayList;
    }

    public ListIterator<Integer> getListIterator() {
        return listIterator;
    }

    public void setListIterator(ListIterator<Integer> listIterator) {
        this.listIterator = listIterator;
    }
}
