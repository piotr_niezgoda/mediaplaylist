package soh.bykowsky;

import java.util.Scanner;

public class UserInterface {

    private IPlayable mainPlaylist;

    public UserInterface(IPlayable playlist) {
        this.mainPlaylist = playlist;
    }

    public void mainAppLoop() {
        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        printUserMenuTitle();
        do {
            printUserMenu();

            System.out.println("Pick your choice");
            int menuOption = scanner.nextInt();

            switch (menuOption) {
                case 1:
                    if (!mainPlaylist.playElement()){
                        System.out.println("No more songs in the playlist");
                    }
                    break;

                case 0:
                    quit = true;
                    break;

                default:
                    quit = true;
                    break;
            }

        } while (!quit);
    }

    private void printUserMenu() {
        System.out.println("1. Play next song");
        System.out.println("0. Quit");
    }

    private void printUserMenuTitle() {
        System.out.println("*** USER MENU ***");
    }
}
