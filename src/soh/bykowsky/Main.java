package soh.bykowsky;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // creating songs and movies
        IPlayable song1 = new Song("Brown sugar", "some album", "Rolling Stone", 666.666);
        IPlayable movie1 = new Movie("Casablanca","some director", 666.66);
        IPlayable song4 = new Song("Hit the lights", "Kill'em All", "Metallica", 666.666);
        IPlayable song5 = new Song("Bad seed", "Reload", "Metallica", 666.666);
        IPlayable song6 = new Song("Blackened", "...And Justice For All", "Metallica", 666.666);
        IPlayable song10 = new Song("Master of Puppets", "Master of Puppets", "Metallica", 666.666);
        IPlayable song11 = new Song("Battery", "Master of Puppets", "Metallica", 666.666);
        IPlayable song12 = new Song("Orion", "Master of Puppets", "Metallica", 666.666);
        IPlayable song7 = new Song("Peace Sells", "Peace Sells... But Who's Buying", "Megadeth", 666.666);
        IPlayable song8 = new Song("Symphony Of Destruction", "Countdown to Extinction", "Megadeth", 666.666);
        IPlayable song9 = new Song("Tornado of Souls", "Rust in Peace", "Megadeth", 666.666);

        List<IPlayable> mediaLibrary = new ArrayList<>();
        mediaLibrary.add(song1);
        mediaLibrary.add(movie1);
        mediaLibrary.add(song4);
        mediaLibrary.add(song5);
        mediaLibrary.add(song6);
        mediaLibrary.add(song7);
        mediaLibrary.add(song8);
        mediaLibrary.add(song9);
        mediaLibrary.add(song10);
        mediaLibrary.add(song11);
        mediaLibrary.add(song12);

        IPlaylist metallicaPlaylist = new Playlist(mediaLibrary, PlaybackMode.RANDOM);
        metallicaPlaylist.addElementToPlaylist(3);
        metallicaPlaylist.addElementToPlaylist(4);
        metallicaPlaylist.addElementToPlaylist(5);
        metallicaPlaylist.addElementToPlaylist(9);
        metallicaPlaylist.addElementToPlaylist(10);
        metallicaPlaylist.addElementToPlaylist(11);

        mediaLibrary.add((IPlayable)metallicaPlaylist);


        IPlaylist mainPlaylist = new Playlist(mediaLibrary);
        mainPlaylist.addElementToPlaylist(1);
        mainPlaylist.addElementToPlaylist(12);
        mainPlaylist.addElementToPlaylist(2);
        mainPlaylist.addElementToPlaylist(6);

        UserInterface userInterface = new UserInterface((IPlayable)mainPlaylist);
        userInterface.mainAppLoop();
    }
}
