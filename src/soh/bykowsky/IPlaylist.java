package soh.bykowsky;

public interface IPlaylist {
    void addElementToPlaylist(int mediaLibraryElementIndexTo);
    void removeElementFromPlaylist(int elementIndexInPlaylist);
}
